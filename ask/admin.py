from django.contrib import admin
from ask.models import question, answer, tag, user_data

admin.site.register(question)
admin.site.register(answer)
admin.site.register(tag)
admin.site.register(user_data)
# Register your models here.
