from django.core.management.base import BaseCommand, CommandError
from django.core.files import File
from ask.models import User, tag, question, answer, user_data
import random

avatar_dir = "uploads/avatars/"

def random_tags(n):
	import urllib2
	url = 'http://randomword.setgetgo.com/get.php'
	c = 0
	for i in range(int(n)):
		data = urllib2.urlopen(url)
		word = data.readlines()[0][:-2]
		t = tag(tagline=word)
		t.save()
		c+=1
		print c

def random_users(n):
	import urllib, os
	nickstart = ["jo", "yolo", "warlockk", "the ", "Chengis", "Vasya", "ninjaw", "pwn", "xakep", "o00o",
			"Twilight ", "Aqua", "john", "lola", "Great ", "nOOb", "Zelenyi", "what",
			"mad", "ttt", "XCOM", "Stepan"]
	nickend = ["1999", "greatest", "master", "mamku_bal", "Khan", "Sparkle", "lool", "Pyatachok", "dwvsdv",
			"random", "man", "the best", "One", "Yoda","ololololo", "whatsanick", "ma", 
			"hofstadter", "Potter-Evans", "the Dragon", "of the War"]
	name = ["John", "Jack", "Petya", "Tim", "Lord"] + 10*['']
	surname = ["Smith", "Khan", "Vasilyev", "the Great"] + 10*['']
	c = 0
	for i in range(n):
		kw = {
			'username': random.choice(nickstart) + random.choice(nickend) + str(random.randint(1,10000)),
			'password': ''.join([random.choice("dkjfslahgdsjfvqwerty") for _ in range(10)]),
			'email': random.choice(nickstart) + str(random.randint(1,10000)) + '@' + random.choice(['guerrillamail.com', 'mail.ru', 'dispostable.com'] + 5*['mailinator.com']),
			'first_name': random.choice(name),
			'last_name': random.choice(surname)
		}
		try: 
			u = User.objects.create_user(**kw)
		except:
			continue
		av = None
		#if (random.randint(1, 100) == 5):
		#	av, _ = urllib.urlretrieve("http://lorempixel.com/100/100")
		#else:
		av = avatar_dir + random.choice(os.listdir(avatar_dir))
		av = File(open(av, 'rw'))
		user_data(avatar=av, rating=random.randint(1,1000), user=u).save()
		print kw['username']
		c += 1
		print(c)
		

def random_questions(n):
	start = ["How to "] * 10 + ["Why ", "When ", "Where to ", "Whence ", "Whereupon ", "Is there a ", "Please "]
	mid = ["do", "buy", "get", "get rid of", "make", "stop", "improve", "kill", "delete", "build", "tame"]
	cont = ["a barrel roll", "a moon park", "file on linux", "my husband", "fnord", "my ex", "meaning of life",
		"an IPhone", "a mountain lion", "a pony", "second season of sherlock", "a TARDIS", "Traxes", "zerg rush"]
	end = ["?", "??", "", "?????", " plleese!", "?.", " just dont tell my mom!", "!!111"]
	textstart = ["Please help me", "I dont know what to do", "I encountered this probled and", "Life is hard and"]
	textend = [" this is killing me", " i hope i will get help", " please please plaese!1"]
	tags = tag
	for i in range(n):
		kw = {
			"title": random.choice(start) + random.choice(mid) + ' ' + random.choice(cont) + random.choice(end) + '(' + str(random.randint(1,100000)) + ')',
			"content": random.choice(textstart) + ' ' + random.choice(textend),
			"author": User.objects.all().order_by('?')[0],
			#"tags": tag.objects.all().order_by('?')[:random.randint(1,10)]
		}
		#try:
		q = question(**kw)
		q.save()
		q.tags.add(*[x for x in tag.objects.all().order_by('?')[:random.randint(1,10)]])
		q.save()
		print kw['title']
		#except:
		#	continue

def random_answers(n):
	for _ in range(n):
		start = ["Do ", "Get ", "Leave ", "Remove ", "Press ", "Don't ", "Suck ", "Use "]
		mid = ["a barrel roll", "your mom", "the configs", "the Force", "fnord"]
		end = [", Luke.", ", noob!", ", man!", ".", " or you're screwed.", "!"]

		kw = {
			"content": random.choice(start) + random.choice(mid) + random.choice(end) + '(' + str(random.randint(1,1000000)) + ')',
			"author": User.objects.all().order_by('?')[0],
			"correct": random.choice([True] + 5 * [False]),
			"quest": question.objects.all().order_by('?')[0]
		}

		a = answer(**kw)
		a.save()
		print kw['content']
		
def fix_tags():
	questions = question.objects.all()
	for q in questions:
	    if len(q.tags.all()) == 0:
		q.tags.add(*[x for x in tag.objects.all().order_by('?')[:random.randint(1,10)]])
		q.save()

def fix_users():
	import urllib, os
	u = User.objects.all()
	u = [x for x in u if not len(user_data.objects.all().filter(user=x))]
	for us in u:
		print us.username
		av = None
		if (random.randint(1, 100) == 5):
			av, _ = urllib.urlretrieve("http://lorempixel.com/100/100")
		else:
			av = avatar_dir + random.choice(os.listdir(avatar_dir))
		av = File(open(av, 'rw'))
		user_data(avatar=av, rating=random.randint(1,1000), user=us).save()
		
class Command(BaseCommand):
	args = '<type> <count>'
	help = 'Fills the database with some loremipsum data'
	
	def add_arguments(self,parser):
		parser.add_argument('type')
		parser.add_argument('count')

	def handle(self, *args, **options):
		type = options['type']
		count = int(options['count'])
		if type == "tags":
			random_tags(count)
		elif type == "users":
			random_users(count)
		elif type == "questions":
			random_questions(count)
		elif type == "answers":
			random_answers(count)
		elif type == "fix_tags":
			fix_tags()
		else:
			fix_users()
